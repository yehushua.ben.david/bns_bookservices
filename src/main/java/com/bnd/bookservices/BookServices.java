package com.bnd.bookservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;



/**
 *
 * @author acelermajer
 */
@SpringBootApplication
public class BookServices {

    public static void main(String[] args) {
       
        SpringApplication.run(BookServices.class, args);
    }
}
