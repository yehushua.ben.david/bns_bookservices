package com.bnd.bookservices.controller;

import com.bnd.bookservices.controller.entity.Search;
import com.bnd.bookservices.core.BookSystem;
import com.bnd.bookservices.core.entity.Book;
import com.bnd.bookservices.core.entity.InfoBook;
import com.bnd.bookservices.core.entity.InfoBookImpl;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author acelermajer
 */
@CrossOrigin
@RestController
public class Main {

    @Autowired
    BookSystem bs;

    @RequestMapping("/")
    public String index() {
        return "index";
    }

    @RequestMapping("/book/{id}")
    public Book book(@PathVariable("id") UUID id) {
        return bs.getBook(id);
    }

    @RequestMapping("/buyBook/{id}")
    public boolean buyBook(@PathVariable("id") UUID id) {
        return bs.buyBook(id);
    }

    @RequestMapping("/books")
    public List<Book> books() {
        return bs.getAvailableBooks();
    }
    
    @RequestMapping("/stock")
    public List<Book> stock() {
        return bs.getAllBooks();
    }

    @PostMapping("/search")
    public List<Book> search(@RequestBody Search search) {
        return bs.searchBooks(search.getPattern().toLowerCase());

    }

    @PostMapping("/addBook")
    public Book addBook(@RequestBody InfoBookImpl book) {
        return bs.addBook(book);
    }
}
