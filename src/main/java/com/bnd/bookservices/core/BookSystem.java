package com.bnd.bookservices.core;

import com.bnd.bookservices.core.entity.Book;
import com.bnd.bookservices.core.entity.BookImpl;
import com.bnd.bookservices.core.entity.InfoBook;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

/**
 *
 * @author acelermajer
 */
@Service
public class BookSystem {

    static List<Book> listOfBook = new ArrayList<>();

    static {
        listOfBook.add(new BookImpl(UUID.randomUUID(), "Robot", "Asimov", 53));
        listOfBook.add(new BookImpl(UUID.randomUUID(), "Robot 2", "Asimov", 15));
        listOfBook.add(new BookImpl(UUID.randomUUID(), "Robot 3", "Asimov", 17));
        listOfBook.add(new BookImpl(UUID.randomUUID(), "Foundation", "Asimov", 70));
        listOfBook.add(new BookImpl(UUID.randomUUID(), "Foundation and Empire", "Asimov", 90));
        listOfBook.add(new BookImpl(UUID.randomUUID(), "Second Foundation", "Asimov", 70));

        listOfBook.add(new BookImpl(UUID.randomUUID(), "Mary Poppins", "P.L. Travers", 7));
        listOfBook.add(new BookImpl(UUID.randomUUID(), "Mary Poppins Comes Back", "P.L. Travers", 0));
        listOfBook.add(new BookImpl(UUID.randomUUID(), "Mary Poppins Opens the Door", "P.L. Travers", 2));
        listOfBook.add(new BookImpl(UUID.randomUUID(), "Mary Poppins in the Park", "P.L. Travers", 1));

        listOfBook.add(new BookImpl(UUID.randomUUID(), "Operating System", "Andrew S. Tanenbaum", 7));

    }

    public synchronized boolean buyBook(UUID id) {
        Book b = this.getBook(id);
        if ((b != null) && (b.getQuantity() > 0)) {
            ((BookImpl) b).setQuantity(b.getQuantity() - 1);
            return true;
        }
        return false;

    }

    public synchronized List<Book> getAllBooks() {
        return listOfBook;
    }

    public synchronized List<Book> getAvailableBooks() {
        return listOfBook.stream().filter((book) -> {
            return book.getQuantity() > 0;
        }).collect(Collectors.toList());
    }

    public synchronized List<Book> searchBooks(String pattern) {

        return listOfBook.stream().filter((book) -> {
            return (book.getQuantity() > 0)
                    && (book.getName().toLowerCase().contains(pattern)
                    || book.getAuthor().toLowerCase().contains(pattern));
        }).collect(Collectors.toList());
    }

    public synchronized Book getBook(UUID id) {
        for (Book b : listOfBook) {
            if (b.getId().equals(id)) {
                return b;
            }
        }
        return null;
    }

    private synchronized Book createBook(InfoBook book) {

        BookImpl b = new BookImpl(UUID.randomUUID(), book.getName(), book.getAuthor(), 0);
        listOfBook.add(b);
        return b;

    }

    public synchronized Book addBook(InfoBook book) {
        Optional<Book> findBook = listOfBook.stream().filter((b) -> {
            return (book.getName().equals(b.getName())
                    && book.getAuthor().equals(b.getAuthor()));
        }).findFirst();

        BookImpl newBook = (BookImpl) findBook.orElse(null);
        if (newBook==null) {
        newBook = (BookImpl) this.createBook(book);
        }
        newBook.setQuantity(newBook.getQuantity() + 1);
        return newBook;

    }

}
