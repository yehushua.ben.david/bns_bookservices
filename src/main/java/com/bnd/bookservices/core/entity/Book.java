/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bnd.bookservices.core.entity;

import java.util.UUID;

/**
 *
 * @author acelermajer
 */
public interface Book extends InfoBook {

    UUID getId();

    int getQuantity();

}
