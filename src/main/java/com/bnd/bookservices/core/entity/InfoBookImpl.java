/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bnd.bookservices.core.entity;

/**
 *
 * @author acelermajer
 */
public class InfoBookImpl implements InfoBook {

    private String name;
    private String author;

    public InfoBookImpl() { 
    } 
    
    
    public InfoBookImpl(String name, String author) {
        this.name = name;
        this.author = author;

    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getAuthor() {
        return this.author;
    }

}
